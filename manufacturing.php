<!doctype html>
<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Manufacturing & Supply Chain </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner13.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner10.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <span class="cus_animate fromRight">AXIA IS</span>
                                    <h2 class="cus_animate fromRight">Global solutions to optimize <br> operations and keep you running<br> efficiently</h2>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">What we Do<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Thought Leadership<span class="arrow"></span></a>
                </div>
            </div>
        </div> <!-- container-->

        <div class="content_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <h3 class="mb-4">Manufacturing & Supply Chain</h3>
                    </div>
                    <div class="col-md-6">
                        <p>The AXIA Consulting Manufacturing & Supply Chain practice simplifies and solves tough business and technology challenges across a wide range of industries and markets, including discrete, process and hybrid manufacturing, high-tech manufacturing, consumer products, automotive, rolled products, and wholesale, retail and e-commerce distribution.</p>
                        <p>With hands-on experience in major industries across six continents, our senior team has the skills and experience to lead and tackle projects on a global scale. Our consultants are experienced in Lean, Six Sigma and many other productivity improvement methodologies, allowing us to customize our approach based on the specific needs of each organization. We quickly adapt to your unique business environment and implement the most effective solution, regardless of software, hardware or process requirements.</p>
                    </div>
                    <div class="col-md-6">
                        <p>We have a proven track record of success assisting with systems integrations for organizations experiencing growth organically and through M&A.  We have assisted clients as client advocates, to represent their best interests and bring experience and leadership when they are engaging large systems integrators and outsourcing partners.  Many of our senior advisors have industry and shop-floor experience, and have, on occasion, back-filled client leadership roles to fill a critical need and bring experience when necessary.</p>
                        <p>No matter your needs, our experienced consultants are able to understand and identify challenges and opportunities within manufacturing and related supply chains. We bring pragmatic solutions to complex challenges.</p>   
                    </div>
                </div> <!-- row -->
            </div> <!-- contentContainer -->
        </div> <!-- content_section -->

        <div class="contentContainer">    
            <div class="d-flex align-items-center justify-content-center">
                <div class="left_image">
                    <img src="images/image19.jpg" />
                </div>
                <div class="right_image">
                    <img src="images/image20.jpg" />
                </div>
            </div>
        </div>  <br/><br/> 

        <!-- Brain Portion -->
        <div class="thought_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Thought Leadership</h2>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="text_content">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <br/><br/>
                            <a href="#" class="btn">READ FULL ARTICLE <span class="arrow"></span></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 d-md-block d-lg-block d-sm-none d-none">
                        <div class="thought_img">
                            <img src="images/image10.jpg" alt="" class="img-right" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brain Portion ends -->
        <div class="content_section bg_grey tech_services">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <h3>Our Manufacturing & Supply Chain services include:</h3><br/>
                        <ul class="list col2 list_dark list-unstyled mb-0">
                            <li>Business/IT Strategy & Alignment</li>
                            <li>Software Selection & Application Portfolio Rationalization</li>                            
                            <li>ERP & MES Software Implementations & Upgrades</li>
                            <li>Business Intelligence Solutions</li>
                            <li>Master Data Management, Data Taxonomy Design <br/>Data Cleansing and Rationalization</li>
                            <li>Business Process Design, Transformation & Improvement</li>
                            <li>Post-Merger Integration </li>
                            <li>Program & Project Management</li>
                            <li>Capital Project Analysis & Cost <br/>Management Strategies </li>
                            <li>Supply Chain Planning, Integration & Optimization</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="full_img_container">
            <div class="w_bg">
                <div class="contentContainer">
                    <img src="images/image18.jpg" alt="" class="img-fluid" />
                </div>
            </div>
        </div>

        <div class="industry_specialisation_sec">
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col-12">
                            <div class="heading_style">
                                <h2>AXIA’s Manufacturing & Supply Chain practice has the skills to tackle<br> 
                                    key business challenges regarding:</h2>
                            </div>
                            <ul class="list col2 list-unstyled">
                                <li>Supply Chain & Inventory Management</li>
                                <li>Warehouse Management</li>
                                <li>Advanced Pricing & Product Configuration</li>
                                <li>E-commerce & Mobile</li>
                                <li>Manufacturing Integration</li>
                                <li>Product Engineering</li>
                                <li>Transportation Management</li>
                                <li>Contract Manufacturing & Outside Processing</li>
                                <li>International & Offshore Manufacturing,<br/>including Maquiladora</li>
                                <li>Quality Management</li>
                                <li>Asset Maintenance</li>
                                <li>Lot Tracking & Genealogy</li>
                                <li>Bar Code & RFID Solutions</li>
                                <li>Product Engineering</li>
                            </ul>
                        </div> <!-- col -->
                    </div> <!-- row -->
                </div> <!-- container-->
            </div> <!-- overlay-->
        </div> <!-- industry_specialisation_sec -->

        <div class="newsEvents_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Other Services</h2>
                        </div>
                    </div> <!-- col -->
                    <div id="newsEvents_counter"></div> 
                    <div class="newsEvents owl-carousel owl-theme">
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/image1.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a></h6>
                                    <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global firm where he served as an Account Manager and worked with.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>                       
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-2.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA named Cincinnati Best Place to Work, Second Consecutive Year</a></h6>
                                    <p>Cincinnati, OH ��AXIA Consulting announces that for the second year in a row, the company has been recognized as one of the Best Places to Work by the Cincinnati Business Courier. To be nominated,.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-3.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Releases Book on Improving Communication Between Project Managers and Developers</a></h6>
                                    <p>COLUMBUS, OH (November 30, 2016) � AXIA Consulting, a global provider of business and technology solutions, announced the release of its book, Perfect Strangers: How Project Managers and Developers Relate and Succeed. Written by AXIA</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                    </div> <!-- carousel END -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- newsEvents_section -->
        
               
        <div class="call_action mb-0">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>To learn more about our industry-specific expertise, <br>visit our seven industry pages or contact us at <a href="#">877-292-5503.</a></p>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- call_action -->

        <?php include("footer.php"); ?>
    </body>
</html>
