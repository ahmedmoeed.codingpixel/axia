<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | News </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header-contact.php"); ?>

        <div class="borderRight border_style"></div>
        <div class="news_header">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <h1>News</h1>
                        <p>AXIA - Business & Technology Axia - Business & Technology Simplified</p>
                    </div>
                </div>
            </div>
        </div> <!-- news header -->

        <div class="contentContainer">
            <div class="news_page">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="news_image">
                            <img src="images/news_image_1.jpg" alt="" />
                        </div>
                    </div> <!-- col -->
                    <div class="col-lg-6">
                        <div class="news_detail">
                            <div class="post_title_wrap">
                                <div class="post_date d-flex align-items-center justify-content-center flex-column"><span class="date">19</span><span class="month">Apr</span></div>
                                <a href="#" class="title">AXIA named as CIncinnati Best Place to Work Finalist, Third Year in a Row</a>
                            </div>
                            <div class="text_content">
                                <p>Cincinnati, OH – For the 3rd year in a row AXIA has been named as a finalist in the Cincinnati Business Courier’s Best Places to Work! This program recognizes Greater Cincinnati employers with the most engaged workforces based on ...</p>
                            </div>
                            <a href="#" class="btn">Read More <span class="arrow"></span></a>
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="news_image">
                            <img src="images/news_image_2.jpg" alt="" />
                        </div>
                    </div> <!-- col -->
                    <div class="col-lg-6">
                        <div class="news_detail">
                            <div class="post_title_wrap">
                                <div class="post_date d-flex align-items-center justify-content-center flex-column"><span class="date">19</span><span class="month">Apr</span></div>
                                <a href="#" class="title">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a>
                            </div>
                            <div class="text_content">
                                <p>Cincinnati, OH – For the 3rd year in a row AXIA has been named as a finalist in the Cincinnati Business Courier’s Best Places to Work! This program recognizes Greater Cincinnati employers with the most engaged workforces based on ...</p>
                            </div>
                            <a href="#" class="btn">Read More <span class="arrow"></span></a>
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="news_image">
                            <img src="images/news_image_3.jpg" alt="" />
                        </div>
                    </div> <!-- col -->
                    <div class="col-lg-6">
                        <div class="news_detail">
                            <div class="post_title_wrap">
                                <div class="post_date d-flex align-items-center justify-content-center flex-column"><span class="date">19</span><span class="month">Apr</span></div>
                                <a href="#" class="title">AXIA’s Greg Pitstick Article, “Next Wave of Technology�? featured in Lead Magazine</a>
                            </div>
                            <div class="text_content">
                                <p>Cincinnati, OH – For the 3rd year in a row AXIA has been named as a finalist in the Cincinnati Business Courier’s Best Places to Work! This program recognizes Greater Cincinnati employers with the most engaged workforces based on ...</p>
                            </div>
                            <a href="#" class="btn">Read More <span class="arrow"></span></a>
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="news_image">
                            <img src="images/news_image_4.jpg" alt="" />
                        </div>
                    </div> <!-- col -->
                    <div class="col-lg-6">
                        <div class="news_detail">
                            <div class="post_title_wrap">
                                <div class="post_date d-flex align-items-center justify-content-center flex-column"><span class="date">19</span><span class="month">Apr</span></div>
                                <a href="#" class="title">AXIA named Cincinnati Best Place to Work,Second Consecutive Year</a>
                            </div>
                            <div class="text_content">
                                <p>Cincinnati, OH – For the 3rd year in a row AXIA has been named as a finalist in the Cincinnati Business Courier’s Best Places to Work! This program recognizes Greater Cincinnati employers with the most engaged workforces based on ...</p>
                            </div>
                            <a href="#" class="btn">Read More <span class="arrow"></span></a>
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="news_image">
                            <img src="images/news_image_5.jpg" alt="" />
                        </div>
                    </div> <!-- col -->
                    <div class="col-lg-6">
                        <div class="news_detail">
                            <div class="post_title_wrap">
                                <div class="post_date d-flex align-items-center justify-content-center flex-column"><span class="date">19</span><span class="month">Apr</span></div>
                                <a href="#" class="title">AXIA Consulting Partners with Excel4Apps to Expedite Business Intelligence Projects with Excel-based Reporting</a>
                            </div>
                            <div class="text_content">
                                <p>Cincinnati, OH – For the 3rd year in a row AXIA has been named as a finalist in the Cincinnati Business Courier’s Best Places to Work! This program recognizes Greater Cincinnati employers with the most engaged workforces based on ...</p>
                            </div>
                            <a href="#" class="btn">Read More <span class="arrow"></span></a>
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- news page -->

            <ul class="pagination justify-content-center mt-4 mb-5">
                <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#">2 </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
            </ul> <!-- pagination -->

        </div> <!-- container -->

        <div class="call_action mb-0">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>To learn more about our industry-specific expertise, <br>visit our seven industry pages or contact us at <a href="#">877-292-5503.</a></p>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container-->
        </div>        

        <?php include("footer.php"); ?>

    </body>
</html>
