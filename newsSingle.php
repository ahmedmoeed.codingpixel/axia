<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | News </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner6.jpg')">
                    </div>
                </div>
                <div class="item">
                   <div class="slide_img" style="background-image: url('images/banner13.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <h2 class="cus_animate fromRight">AXIA Consulting Introduces <br/>Nick Lemoine as the Newest Member <br/>of its Business Development Team</h2>
                                    <p class="text_weight_black cus_animate fromRight"> Posted on January 29, 2018</p>                                    
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="contentContainer">
            <div class="news_detail_page">
                <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global Tax Consulting firm where he served as an Account Manager and worked with with Fortune 500 clients across a wide breadth of industries. As a new member of the AXIA’s business development team, Nick will be an additional contact for organizations who need world class industry resources to deliver projects on-time, within budget, and in-scope.</p>
                <p>“I’m excited to be joining AXIA consulting,�? said Nick. “Their passion for client service aligns with my own and I’m looking forward to deepening the relationships they’ve created and helping to spread the word regarding the great work they do.�?</p>
                <p>Nick received his degree in Economics from the Fisher College of Business at The Ohio State University and lives in Central Ohio with his wife and two daughters.</p>
                <h4>Methodology</h4>
                <p>The 2016 Inc. 5000 is ranked according to percentage revenue growth when comparing 2012 to 2015. To qualify, companies must have been founded and generating revenue by March 31, 2012. They had to be U.S.-based, privately held, for profit, and independent—not subsidiaries or divisions of other companies—as of December 31, 2015. (Since then, a number of companies on the list have gone public or been acquired.) The minimum revenue required for 2012 is $100,000; the minimum for 2015 is $2 million. As always, Inc. reserves the right to decline applicants for subjective reasons. Companies on the Inc. 500 are featured in Inc.’s September issue. They represent the top tier of the Inc. 5000, which can be found at <br/> 
                    <a href="#">http://www.inc.com/inc5000.</a></p>
                <h4>AXIA’s Jay Canupp named Chair of Oracle Business Intelligence / Big Data Special Interest Group</h4>

                <p>AXIA Consulting’s Jay Canupp has been appointed Chair of the National Oracle Business Intelligence / Big Data Special Interest Group (OBI-BD SIG). As an affiliate of OAUG, the OBI-BD SIG mission is to provide education & networking opportunities for all Oracle BI & Big Data subject matter to it’s over 2,000 members from more than 60 different countries.</p>
                <p>Jay is a senior consultant with over sixteen years of experience serving in various roles on Business Intelligence projects within the Utilities, Software, Manufacturing, Insurance, and Public Sector industries. He has experience in implementing operational and analytical reporting solutions within the Purchasing, Manufacturing, Financials, Inventory and Human Resources areas and has also led multiple enterprise reporting implementations using a variety of tools such as OBIEE, Discoverer, FSG, and Business Intelligence Publisher.</p>
                <p><strong>Congratulations Jay!</strong></p>
            </div> <!-- news page -->
        </div> <!-- container -->

        <?php include("footer.php"); ?>

    </body>
</html>
