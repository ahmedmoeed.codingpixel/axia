<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | LeaderShip </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner4.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner12.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <h2 class="cus_animate fromRight">Leadership</h2>
                                    <p class="cus_animate fromRight">Dedicated leaders committed to building great teams and solving tough problems</p>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">Learn about our Services<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Access our Assessments, Offerings & Thought Leadership<span class="arrow"></span></a>
                </div>
            </div>
        </div>

        <div class="leadership_section">
            <div class="contentContainer">
                <section>
                    <p>Our leadership team is guided by a simple vision: to grow one great consultant at a time, to work for great clients, and to do the work that we love. Each of our leaders lives by this vision and has dedicated their careers to providing clients with proven solutions to meet the latest business challenges.</p>
                </section>                    
                <section>
                    <h3>Company Leadership</h3>
                    <ul class="member_list list-unstyled">
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_1.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Ed Mueller</span>
                                    <span class="designation">CEO</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_2.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Paul Grove</span>
                                    <span class="designation">President & CFO</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_3.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Tony Kopyar</span>
                                    <span class="designation">VP of Human Resources</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_4.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Ed Jon Riley</span>
                                    <span class="designation">EVP & Director of Oracle Services</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_5.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Brad Norkin</span>
                                    <span class="designation">EVP</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </section> <!-- section -->
                <section>
                    <h3>Practice Leadership</h3>
                    <ul class="member_list list-unstyled">
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_6.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">John Tierney</span>
                                    <span class="designation">Director of Client Advisory</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_4.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Jon Riley</span>
                                    <span class="designation">EVP & Director of Oracle Services</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_7.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Cliff Wilson</span>
                                    <span class="designation">Director of JD Edwards Services</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_8.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Kevin Colwell</span>
                                    <span class="designation">Director of Government & Education Services</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_9.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Greg Pitstick</span>
                                    <span class="designation">Director of Global Mergers & Acquisitions</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </section> <!-- section -->
                <section>
                    <h3>Business Development Team</h3>
                    <ul class="member_list list-unstyled">
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_10.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Teresa Conroy-Roth</span>
                                    <span class="designation">Business Development Director</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_11.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Harry Kiefaber</span>
                                    <span class="designation">Business Development Director</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="team_member">
                                <div class="image">
                                    <img src="images/team_member_12.jpg" alt="image" />
                                </div>
                                <div class="memberInfo">
                                    <span class="name">Nick Lemoine</span>
                                    <span class="designation">Business Development Manager</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </section> <!-- section -->
            </div> <!-- container -->
        </div>

        <?php include("footer.php"); ?>

    </body>
</html>
