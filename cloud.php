<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Cloud </title>        
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner10.jpg')">
                    </div>
                </div>
                <div class="item">
                   <div class="slide_img" style="background-image: url('images/banner12.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <h2 class="cus_animate fromRight">Cloud</h2>
                                    <p class="cus_animate fromRight">Translating the cloud to your business environment</p>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">Cloud Services<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Thought Leadership<span class="arrow"></span></a>
                </div>
            </div>
        </div>

        <div class="content_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <h2>The Challenge</h2><br/>
                        <p>AXIA has extensive implementation and advisory experience when it comes to cloud-based solutions.  Whether you are considering the cloud for the first time or are looking to expand your use of cloud applications, AXIA’s senior client advisors will provide strategic planning and implementation services to help you navigate your journey to the cloud. Our expert team has in-depth experience delivering cloud-based solutions for complex challenges in every business area and industry.  With an emphasis on maximizing investments and planning for the long-term, we partner with your business to deliver meaningful results to meet your project and organizational goals.</p>
                        <p>Our deep understanding of business and cloud technologies allows us to work seamlessly with your team to understand your environment and business requirements. By taking into account factors such as security, functionality, vendor alignment, cost and more, we are able to determine if a cloud solution makes sense for your organization and will recommend the best software based on your needs. From planning and software selection to implementation and data migration and training, AXIA consultants are there to guide you through the process.</p>
                        <br/>
                        <p><strong>AXIA Cloud service offerings include:</strong></p>
                        <ul class="list col3 list_dark list-unstyled">
                            <li>Consulting & Strategy</li>
                            <li>Software & Vendor Selection</li>
                            <li>Implementation</li>
                            <li>System Integration</li>
                            <li>Data Migration & Conversions</li>
                            <li>Multinational Rollouts</li>
                            <li>Program & Project Management</li>
                            <li>System Upgrades & Expansions</li>
                            <li>Application Performance Monitoring</li>
                            <li>Training & Support</li>
                            <li>Change Management</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Brain Portion -->
        <div class="thought_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Thought Leadership</h2>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="text_content">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <br/><br/>
                            <a href="#" class="btn">READ FULL ARTICLE <span class="arrow"></span></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 d-md-block d-lg-block d-sm-none d-none">
                        <div class="thought_img">
                            <img src="images/image10.jpg" alt="" class="img-right" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brain Portion ends -->

        <div class="content_section bg_grey">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="text_medium">
                            <p>We have extensive experience with a variety of cloud software, including, but not limited to:</p>
                        </div>
                        <br/>
                        <ul class="list col3 list_dark list-unstyled">
                            <li>Microsoft Dynamics 365 (AX)</li>
                            <li>NetSuite</li>
                            <li>Oracle Cloud Applications</li>
                            <li>Amazon Web Services (AWS)</li>
                            <li>ServiceNow</li>
                            <li>SAP Hybris</li>
                            <li>Salesforce</li>
                            <li>SharePoint</li>
                            <li>Concur</li>
                            <li>Workday</li>
                            <li>Ceridian Dayforce HCM</li>
                            <li>Rackspace</li>
                            <li>Comply365</li>
                            <li>VisionDSM</li>
                            <li>ZS-JAMS</li>
                            <li>ShopKeep POS</li>
                            <li>Trimble</li>
                            <li>Xyleme LCMS</li>
                            <li>FinancialForce</li>
                            <li>Opvantek xDR</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="newsEvents_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Other Services</h2>
                        </div>
                    </div> <!-- col -->
                    <div id="newsEvents_counter"></div> 
                    <div class="newsEvents owl-carousel owl-theme">
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/image1.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a></h6>
                                    <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global firm where he served as an Account Manager and worked with.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>                       
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-2.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA named Cincinnati Best Place to Work, Second Consecutive Year</a></h6>
                                    <p>Cincinnati, OH ��AXIA Consulting announces that for the second year in a row, the company has been recognized as one of the Best Places to Work by the Cincinnati Business Courier. To be nominated,.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-3.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Releases Book on Improving Communication Between Project Managers and Developers</a></h6>
                                    <p>COLUMBUS, OH (November 30, 2016) � AXIA Consulting, a global provider of business and technology solutions, announced the release of its book, Perfect Strangers: How Project Managers and Developers Relate and Succeed. Written by AXIA</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                    </div> <!-- carousel END -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- newsEvents_section -->


        <div class="call_action mb-0">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>To learn more about our industry-specific expertise, <br>visit our seven industry pages or contact us at <a href="#">877-292-5503.</a></p>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container-->
        </div>

        <?php include("footer.php"); ?>

    </body>
</html>
