<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Industries </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/homebanner1.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner8.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <span class="cus_animate fromRight">AXIA IS</span>
                                    <h2 class="cus_animate fromRight"> Expert business and <br/> technology consulting <br/> for global industries</h2>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="banner_links">
            <div class="contentContainer">
                <div class="left cus_animate fromBottom">
                    <a href="#" class="link">What we Do<span class="arrow"></span></a>
                </div>
                <div class="right cus_animate fromBottom">
                    <a href="#" class="link">Why AXIA?<span class="arrow"></span></a>
                </div>
            </div>
        </div> <!-- container-->

        <div class="categories_drop_down">
            <div class="contentContainer">
                <div class="row">
                    <div class="col">
                        <div class="parent_menu_item">
                            <span class="open_cat">Industries Served <i class="fas fa-angle-down"></i></span>
                        </div>
                        <div class="categories_list col-10 offset-md-1">
                            <ul class="d-flex flex-wrap">
                                <li><a href="#">Technology</a></li>
                                <li><a href="#">Manufacturing & Supply Chain</a></li>
                                <li><a href="#">Utilities & Energy</a></li>
                                <li><a href="#">Government & Higher Education</a></li>
                                <li><a href="#">Retail & eCommerce</a></li>
                                <li><a href="#">Financial Services</a></li>
                                <li><a href="#">Healthcare</a></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- categories_drop_down -->

        <div class="industry_intro">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-md-7">
                        <div class="left_side">
                            <div class="txt_content">
                                <div class="section_title">
                                    <h2>Industries Served</h2>
                                </div>
                                <p>AXIA Consulting simplifies and solves the toughest business and technology challenges through extensive experience in numerous industries. A consulting firm without the hype, we’re 100% focused on providing meaningful results that impact your bottom line and maximize your investment.</p>
                                <p>With each consultant bringing more than 20 years of experience, our senior team has in-depth, hands-on understanding of the industries we serve. We take the time to understand the intricacies of your organization and apply our industry-focused approach to solve your specific business and technology challenges.</p>
                                <p>Whether your needs are local or global, our team can help by applying the expertise we have gained through delivering business and technology projects in more than 54 countries throughout six continents.</p>
                            </div>
                            <div class="image">
                                <img src="images/image5.jpg" alt="" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 d-flex align-items-center justify-content-center">
                        <div class="right_side">
                            <img src="images/image6.jpg" alt="" class="img-fluid" />
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- container-->
        </div>

        <div class="full_img_container no_overlap">
            <div class="w_bg">
                <div class="contentContainer">
                    <img src="images/image4.jpg" alt="" class="img-fluid" />
                </div>
            </div>
        </div>
        
        <div class="industry_specialisation_sec">
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col-12">
                            <div class="heading_style">
                                <h2>We specialize in serving the <br>following industries:</h2>
                            </div>
                            <ul class="specialisation_list list-unstyled">
                                <li><div class="icon"><img src="images/icon1.png" /></div> Technology </li>
                                <li><div class="icon"><img src="images/icon2.png" /></div> Manufacturing & Supply Chain</li>
                                <li><div class="icon"><img src="images/icon3.png" /></div> Utilities & Energy</li>
                                <li><div class="icon"><img src="images/icon4.png" /></div> Government & Higher Education</li>
                                <li><div class="icon"><img src="images/icon5.png" /></div> Retail & eCommerce</li>
                                <li><div class="icon"><img src="images/icon6.png" /></div> Financial Services</li>
                                <li><div class="icon"><img src="images/icon7.png" /></div> Healthcare</li>
                            </ul>
                        </div> <!-- col -->
                    </div> <!-- row -->
                </div> <!-- container-->
            </div> <!-- overlay-->
        </div> <!-- industry_specialisation_sec -->


        <div class="ourservices_section jump_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>What We Do</h2>
                        </div>
                        <ul class="services_tab_list list-unstyled clearfix">
                            <li data-num="1" class="active"><a href="#">Cloud</a></li>
                            <li data-num="2"><a href="#">Technology</a></li>
                            <li data-num="3"><a href="#">Data</a></li>
                            <li data-num="4"><a href="#">Business Consulting</a></li>
                            <li data-num="5"><a href="#">Organizational Change Management</a></li>
                            <li data-num="6"><a href="#">M & A</a></li>
                            <li data-num="7"><a href="#">Program Management </a></li>
                            <li data-num="8"><a href="#">Innovation</a></li>
                        </ul>
                        <div class="services_tab_slider">
                            <div class="service_images">
                                <img src="images/services-spacer.png" class="spacer">
                                <div class="service_image show" data-num="1">
                                    <img src="images/banner4.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="2">
                                    <img src="images/banner5.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="3">
                                    <img src="images/banner6.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="4">
                                    <img src="images/banner7.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="5">
                                    <img src="images/banner8.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="6">
                                    <img src="images/banner9.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="7">
                                    <img src="images/banner10.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="8">
                                    <img src="images/banner11.jpg" alt="image" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="service_small_images">
                                <div class="image show" data-num="1">
                                    <img src="images/banner5.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="2">
                                    <img src="images/banner6.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="3">
                                    <img src="images/banner7.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="4">
                                    <img src="images/banner8.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="5">
                                    <img src="images/banner9.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="6">
                                    <img src="images/banner10.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="7">
                                    <img src="images/banner11.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="8">
                                    <img src="images/banner12.jpg" alt="image" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="services_text">
                                <div class="text show" data-num="1">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="2">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="3">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="4">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="5">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="6">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="7">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="8">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sliding-cover"></div>
                        </div>

                        <div class="servicesMobile">
                            <div class="service">
                                <div class="image">
                                    <img src="images/banner4.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                    </div>
                                    <div class="col-4">
                                        <a href="#" class="btn">LEARN MORE<span class="arrow"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- Our Services section -->

        <div class="newsEvents_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Other Services:</h2>
                        </div>
                    </div> <!-- col -->
                    <div id="newsEvents_counter"></div> 
                    <div class="newsEvents owl-carousel owl-theme">
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/image1.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a></h6>
                                    <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global firm where he served as an Account Manager and worked with.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>                       
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-2.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA named Cincinnati Best Place to Work, Second Consecutive Year</a></h6>
                                    <p>Cincinnati, OH ��AXIA Consulting announces that for the second year in a row, the company has been recognized as one of the Best Places to Work by the Cincinnati Business Courier. To be nominated,.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-3.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Releases Book on Improving Communication Between Project Managers and Developers</a></h6>
                                    <p>COLUMBUS, OH (November 30, 2016) � AXIA Consulting, a global provider of business and technology solutions, announced the release of its book, Perfect Strangers: How Project Managers and Developers Relate and Succeed. Written by AXIA</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                    </div> <!-- carousel END -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- newsEvents_section -->


        <div class="call_action mb-0">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>To learn more about our industry-specific expertise, <br/>visit our seven industry pages or contact us at <a href="#">877-292-5503.</a></p>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- industry_specialisation_sec -->

        <div class="newsEvents_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>AXIA on the Move</h2>
                        </div>
                    </div> <!-- col -->
                    <div id="newsEvents_counter"></div> 
                    <div class="newsEvents owl-carousel owl-theme">
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/image1.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a></h6>
                                    <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global firm where he served as an Account Manager and worked with.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>                       
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-2.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA named Cincinnati Best Place to Work, Second Consecutive Year</a></h6>
                                    <p>Cincinnati, OH ��AXIA Consulting announces that for the second year in a row, the company has been recognized as one of the Best Places to Work by the Cincinnati Business Courier. To be nominated,.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-3.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Releases Book on Improving Communication Between Project Managers and Developers</a></h6>
                                    <p>COLUMBUS, OH (November 30, 2016) � AXIA Consulting, a global provider of business and technology solutions, announced the release of its book, Perfect Strangers: How Project Managers and Developers Relate and Succeed. Written by AXIA</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                    </div> <!-- carousel END -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- newsEvents_section -->

        <?php include("footer.php"); ?>

    </body>
</html>
