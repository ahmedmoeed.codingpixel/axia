
        <header class="header">
            <div class="logo">
                <a href="index.php">
                    <img src="images/logo.png" alt="Logo AXIA"/>
                </a>
            </div>
            <div class="stickmenubar"></div>
        </header>

        <div class="main_menu">
            <div class="menu_btn">
                <span class="menuicon"> 
                    <span class="top"></span>
                    <span class="medium"></span>
                    <span class="bot"></span>
                </span> 
                <span class="txt">Menu</span>
            </div>
            <div class="back_overlay"></div>
            <div class="menu_wrap">
                <div class="logo_menu_desktop">
                    <a href="index.html">
                        <img src="images/logo.png" alt="Logo AXIA"/>
                    </a>
                </div>
                <div class="menu-scroller">
                    <div class="menu-contents">
                        <div class="menu_item_wrap">
                            <div class="logo_menu_mobile">
                                <a href="index.html">
                                    <img src="images/logo.png" alt="Logo AXIA"/>
                                </a>
                            </div>
                            <div class="row">
                                <div class="col left">
                                    <nav class="menu-nav">
                                        <ul>
                                            <li> <a href="#">Our Company</a> </li>
                                            <li>
                                                <a href="#">Our Projects</a>
                                                <ul>
                                                    <li> <a href="#">Project Name</a> </li>
                                                    <li> <a href="#">Project Name</a> </li>
                                                    <li> <a href="#">Project Name</a> </li>
                                                    <li> <a href="#">Project Name</a> </li>                                       
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#">Services</a>
                                                <ul>
                                                    <li> <a href="">Service Name</a> </li>
                                                    <li> <a href="">Service Name</a> </li>
                                                    <li> <a href="">Service Name</a> </li>
                                                    <li> <a href="">Service Name</a> </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="col right">
                                    <nav class="menu-nav">
                                        <ul>
                                            <li> <a href="#">News</a> </li>
                                            <li>
                                                <a href="#">Contact</a>
                                                <ul>
                                                    <li> <a href="#">Terms & Conditons</a> </li>
                                                    <li> <a href="#">Privacy Policy</a> </li>                                   
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                    <div class="designedBy">
                                        <p>Designed & Developed by <br/><a href="#">CodingPixel</a></p>
                                    </div>
                                    <nav class="social">
                                        <ul class="social_icon list-unstyled">
                                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
