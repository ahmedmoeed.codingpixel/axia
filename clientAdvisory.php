<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Client Advisory </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                <div class="slide_img" style="background-image: url('images/banner12.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner10.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <span class="cus_animate fromRight">AXIA IS</span>
                                    <h2 class="cus_animate fromRight">Trusted partners who help you tackle <br/>your toughest business and <br/>technology challenges</h2>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->


        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">Client Advisory Services<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Thought Leadership<span class="arrow"></span></a>
                </div>
            </div>
        </div> <!-- container-->

        <div class="content_section">
            <div class="contentContainer">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <br/>
                        <h3 class="mb-3">Client Advisory</h3> <br/>
                        <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                        <p>With a deep understanding of business and technology, our senior consultants work seamlessly with you and your team to understand your environment and deliver high-impact results. All of our consultants have extensive experience delivering solutions for complex challenges in every area of business, from program management to post merger integration and enterprise data management.</p>
                        <p>As a consulting firm without the hype, we are dedicated to providing meaningful results to boost your bottom line and maximize your investments.</p>
                    </div>
                    <div class="col-md-6">
                        <img src="images/image16.jpg" alt="image" />
                    </div>
                    <div class="col-12 mt-5">
                        <img src="images/image17.jpg" alt="image" />
                    </div>
                </div> <!-- row -->
            </div> <!-- contentContainer -->
        </div> <!-- content_section -->

        <!-- Brain Portion -->
        <div class="thought_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Thought Leadership</h2>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="text_content">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <br/><br/>
                            <a href="#" class="btn">READ FULL ARTICLE <span class="arrow"></span></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 d-md-block d-lg-block d-sm-none d-none">
                        <div class="thought_img">
                            <img src="images/image10.jpg" alt="" class="img-right" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brain Portion ends -->

        <div class="content_section bg_grey">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <h3>AXIA Client Advisory services include:</h3><br/>
                        <ul class="list col2 list_dark list-unstyled mb-0">
                            <li>Program Management</li>
                            <li>Organizational Transformation</li>                            
                            <li>Business Advisory</li>
                            <li>Technology Services</li>
                            <li>Enterprise Information Management</li>
                            <li>Mergers and Acquisitions</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="newsEvents_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Other Services</h2>
                        </div>
                    </div> <!-- col -->
                    <div id="newsEvents_counter"></div> 
                    <div class="newsEvents owl-carousel owl-theme">
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/image1.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a></h6>
                                    <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global firm where he served as an Account Manager and worked with.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>                       
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-2.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA named Cincinnati Best Place to Work, Second Consecutive Year</a></h6>
                                    <p>Cincinnati, OH ��AXIA Consulting announces that for the second year in a row, the company has been recognized as one of the Best Places to Work by the Cincinnati Business Courier. To be nominated,.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-3.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Releases Book on Improving Communication Between Project Managers and Developers</a></h6>
                                    <p>COLUMBUS, OH (November 30, 2016) � AXIA Consulting, a global provider of business and technology solutions, announced the release of its book, Perfect Strangers: How Project Managers and Developers Relate and Succeed. Written by AXIA</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                    </div> <!-- carousel END -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- newsEvents_section -->
        
        <div class="call_action mb-0">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>To learn more about our industry-specific expertise, <br>visit our seven industry pages or contact us at <a href="#">877-292-5503.</a></p>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- call_action -->

        <?php include("footer.php"); ?>
        
    </body>
</html>
