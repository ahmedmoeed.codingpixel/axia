<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Careers </title>
        <?php include("assets.php"); ?>
    </head>

    <body>
        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner8.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner9.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <h2 class="cus_animate fromRight">Careers</h2>
                                    <p class="cus_animate fromRight">Accelerate your Career at one of the Best Places to Work in the Technology Industry</p>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">Open Positions<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Community Involvement<span class="arrow"></span></a>
                </div>
            </div>
        </div>

        <div class="content_section">
            <div class="contentContainer">
                <div class="TextImg">
                    <div class="row">
                        <div class="col-lg-7 col-12 group_image">
                            <img src="images/image13.jpg" alt="" />
                        </div>
                        <div class="col-lg-5 d-flex align-items-center">
                            <div class="text_content">
                                <h3>Accelerate your Career at one of the Best Places to Work in the Technology Industry</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p>AXIA is actively seeking problem solvers who enjoy using their technology expertise to solve complex business problems.</p>
                        <p>As an employee-owned company, the AXIA team is full of self-motivated individuals who take initiative and are creative, flexible and collaborative. Our consultants work with clients all across the United States and in over 54 countries, and we pride ourselves on helping them tackle their toughest business and technology challenges.</p>
                        <p>Even though we work hard, it’s important for us to come together and have fun. Community service projects, happy hours, networking events and company meetings are just a few examples of how a strong company culture and shared set of values keep our team connected no matter their location.</p>
                        <p>Sound appealing? If so, then AXIA may be the ideal fit for you.</p>
                        <br/>
                        <h3>Why Work at AXIA?</h3>
                        <br/>
                        <p>We think it comes down to our people. They are what makes AXIA a unique and rewarding place to work. Our fun, friendly and knowledgeable team is the reason behind why AXIA is an eight-time Best Places to Work award winner and a seven-time Inc. 500 honoree.</p>
                        <p>We don’t want you to take our word for it. Here are the top 10 reasons why our team loves working at AXIA:</p>
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7 col-md-6">
                        <br/><br/>
                        <ol>
                            <li>Our team comes first and is like one big family</li>
                            <li>We like to have fun at our company meetings, held twice a year in different cities, which are followed by events such as rooftop parties, riverboat cruises, casino nights and much more</li>
                            <li>Focus on community. Our team gives back to the communities we live in by participating in and donating to charitable initiatives across the country. We also have an employee donation matching program</li>
                            <li>Values-based culture focused on authenticity and approachability</li>
                            <li>Formalized mentoring and professional development program</li>
                            <li>Competitive salary and benefits package</li>
                            <li>We are an ESOP, an Employee Stock Ownership Plan, which means you will have an input in shaping the company’s future</li>
                            <li>Independence to solve problems creatively with the strong support of a team</li>
                            <li>Hands-on, accessible leadership</li>
                            <li>A 95% retention rate, one of the highest in the technology industry</li>
                        </ol>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <img src="images/image12.jpg" alt="" />
                    </div>
                </div>
            </div>
        </div>

        <div class="content_section bg_grey">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <h2>Benefits</h2><br/>
                        <p><strong>Supporting your health and financial needs with a comprehensive benefits package helps ensure you have what you need to enjoy your life at work and home.</strong></p>
                        <p><strong>Our benefits program includes:</strong></p>
                        <br/>
                        <ul class="list list_dark col2 list-unstyled">
                            <li>Medical, dental, vision and prescription drug coverage</li>
                            <li>Employee stock ownership plan</li>
                            <li>401(k) savings plan</li>
                            <li>Health Savings Account</li>
                            <li>Short- and long-term disability coverage</li>
                            <li>Paid time off</li>
                            <li>Life insurance</li>
                            <li>Employee charity donation matching program</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="call_action_careers">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-lg-8 col-12">
                        <h2>Career Opportunities</h2>
                        <p>For more information about employment opportunities.</p>
                    </div>
                    <div class="col-lg-4">
                        <a href="#" class="btn btn-white">Open Positions<span class="arrow"></span></a>
                    </div>
                </div>
            </div>
        </div>

        <?php include("footer.php"); ?>
    </body>
</html>
