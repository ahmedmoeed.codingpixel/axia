<footer class="footer">
    <div class="overlay">
        <div class="contentContainer">
            <div class="row">
                <div class="col">
                    <div class="footer-logo">
                        <img src="images/logo-footer.png" alt="Logo AXIA" />
                    </div>
                </div> <!-- col -->
            </div> <!-- row -->
            <div class="top_sec">
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="footer_widget widget_about">
                            <h3>Business & <br />Technology <br />Simplified.</h3>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="footer_widget">
                            <h4>About us</h4>
                            <p>AXIA Consulting is a premier consulting firm that provides technology and business consulting services. AXIA Consulting was founded to bring highly experienced consulting resources to help address our client’s challenges; implement business and technology efficiencies; and deliver bottom line results.</p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
                        <div class="footer_widget widget_address">
                            <h4>Headquarters</h4>

                            <p>1391 West Fifth Avenue<br /> Suite 320Columbus, OH 43212</p>
                            <p>Toll Free: <a href="tel:866-937-5550">866-937-5550</a></p>
                            <p>Local/Fax: 614-675-4050</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="footer_widget">
                            <h4>Other Links</h4>
                            <ul class="quick_links list-unstyled clearfix mb-0">
                                <li><a href="#">Industries</a></li>
                                <li><a href="#">Client Advisory</a></li>
                                <li><a href="#">Cloud</a></li>
                                <li><a href="#">Oracle</a></li>
                                <li><a href="#">JD Edwards</a></li>
                                <li><a href="#">Company</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">News</a></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- row -->
                <div class="row copyright_row">
                    <div class="col-sm-7">
                        <div class="copyright">
                            <p>Copyright © 2018 AXIA Consulting, Incorporated</p>
                            <p>Designed and Developed by <a href="#">CodingPixel</a></p>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <ul class="social_icon list-unstyled clearfix mb-0">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div> <!-- row -->
            </div> <!-- top sec -->
        </div> <!-- container-->
    </div> <!-- overlay-->
</footer> <!-- footer-->

<div class="loadingClose">
    <div class="layer1">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 176.9 511.07"><path style="fill: #1f2a2a" d="M176.78,506.66l-5.54,4.41c-4.64-5.44-9.28-10.74-13.79-16.17q-23.53-28.33-47-56.76Q87.63,410.51,64.8,382.87c-13.66-16.49-27.13-33.15-41.11-49.37C8.83,316.27-.47,297.07,0,273.86c.35-16.44.21-32.91,1.1-49.32C2,208.31,9.9,194.8,20,182.56q43.62-52.78,87.36-105.48,30.48-36.82,60.89-73.71c.85-1,1.55-2.15,2.42-3.37l6.21,4.56c-8.34,11.8-16.6,23.54-24.9,35.25C115.69,91.1,79.49,142.47,43,193.6,33.27,207.16,28.9,222,28.84,238.39c0,10.66.21,21.33-.11,32-.6,19.63,6,36.54,17.52,52.25,18.66,25.38,36.62,51.26,54.84,77q36.39,51.32,72.73,102.69C174.78,503.63,175.68,505,176.78,506.66Z"/></svg>
    </div>
    <div class="layer2">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 176.71 510.8"><path style="fill: #95b186" d="M.15,4.34,5.77,0C12.1,7.5,18.32,14.79,24.45,22.16q21.15,25.47,42.23,51,27.75,33.57,55.47,67.19,18.69,22.66,37.32,45.38a74.19,74.19,0,0,1,17.21,47.8c.07,17.28,0,34.57-.72,51.83-.72,16.23-8.26,30-18.33,42.26-20.93,25.55-42.12,50.9-63.17,76.36q-32.52,39.33-65,78.69C21.73,492,13.89,501.22,5.84,510.8L0,506.39C19,479.58,37.93,453,56.8,426.3c25.67-36.26,51.2-72.63,77-108.78,8-11.27,13.48-23.41,13.87-37.23.47-16.65.42-33.33,0-50-.4-16.31-8.34-29.67-17.6-42.55C108.24,157.33,86.66,126.7,65,96.13Q34.06,52.51,3.19,8.86C2.16,7.4,1.19,5.89.15,4.34Z"/></svg>
    </div>
</div>
<div class="loadingOpen">
    <div class="layer1">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 176.9 511.07"><path style="fill: #1f2a2a" d="M176.78,506.66l-5.54,4.41c-4.64-5.44-9.28-10.74-13.79-16.17q-23.53-28.33-47-56.76Q87.63,410.51,64.8,382.87c-13.66-16.49-27.13-33.15-41.11-49.37C8.83,316.27-.47,297.07,0,273.86c.35-16.44.21-32.91,1.1-49.32C2,208.31,9.9,194.8,20,182.56q43.62-52.78,87.36-105.48,30.48-36.82,60.89-73.71c.85-1,1.55-2.15,2.42-3.37l6.21,4.56c-8.34,11.8-16.6,23.54-24.9,35.25C115.69,91.1,79.49,142.47,43,193.6,33.27,207.16,28.9,222,28.84,238.39c0,10.66.21,21.33-.11,32-.6,19.63,6,36.54,17.52,52.25,18.66,25.38,36.62,51.26,54.84,77q36.39,51.32,72.73,102.69C174.78,503.63,175.68,505,176.78,506.66Z"/></svg>
    </div>
    <div class="layer2">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 176.71 510.8"><path style="fill: #95b186" d="M.15,4.34,5.77,0C12.1,7.5,18.32,14.79,24.45,22.16q21.15,25.47,42.23,51,27.75,33.57,55.47,67.19,18.69,22.66,37.32,45.38a74.19,74.19,0,0,1,17.21,47.8c.07,17.28,0,34.57-.72,51.83-.72,16.23-8.26,30-18.33,42.26-20.93,25.55-42.12,50.9-63.17,76.36q-32.52,39.33-65,78.69C21.73,492,13.89,501.22,5.84,510.8L0,506.39C19,479.58,37.93,453,56.8,426.3c25.67-36.26,51.2-72.63,77-108.78,8-11.27,13.48-23.41,13.87-37.23.47-16.65.42-33.33,0-50-.4-16.31-8.34-29.67-17.6-42.55C108.24,157.33,86.66,126.7,65,96.13Q34.06,52.51,3.19,8.86C2.16,7.4,1.19,5.89.15,4.34Z"/></svg>
    </div>
</div>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>
<script src="js/TweenMax.min.js"></script>