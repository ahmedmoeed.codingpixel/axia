<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Events </title>
        <?php include("assets.php"); ?>       
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner5.jpg')">
                    </div>
                </div>
                <div class="item">
                    <img src="./images/banner8.jpg" />
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <h2 class="cus_animate fromRight">Business & <br/> Technology <br/>Simplified.</h2>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">What we Do<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Thought Leadership<span class="arrow"></span></a>
                </div>
            </div>
        </div>

        <div class="contentContainer">
            <div class="events_listing_page">
                <div class="heading_style black">
                    <h2>Conferences <br> Events</h2>
                </div>
                <ul class="event_list d-flex list-unstyled justify-content-between">
                    <li>
                        <div class="event_wrap d-flex align-items-center">
                            <div class="event_image">
                                <img src="images/event_1.jpg" alt="" />
                            </div>
                            <div class="event_info">
                                <span class="date">September 12, 2018</span>
                                <a href="#" class="name">Indiana IT Symposium</a>
                                <span class="location"><i class="fas fa-map-marker-alt"></i> Indianapolis, IN</span>
                                <a href="#" class="btn">Learn More <span class="arrow"></span></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="event_wrap d-flex align-items-center">
                            <div class="event_image">
                                <img src="images/event_2.jpg" alt="" />
                            </div>
                            <div class="event_info">
                                <span class="date">September 12, 2018</span>
                                <a href="#" class="name">Indiana IT Symposium</a>
                                <span class="location"><i class="fas fa-map-marker-alt"></i> Indianapolis, IN</span>
                                <a href="#" class="btn">Learn More <span class="arrow"></span></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="event_wrap d-flex align-items-center">
                            <div class="event_image">
                                <img src="images/event_3.jpg" alt="" />
                            </div>
                            <div class="event_info">
                                <span class="date">September 12, 2018</span>
                                <a href="#" class="name">Indiana IT Symposium</a>
                                <span class="location"><i class="fas fa-map-marker-alt"></i> Indianapolis, IN</span>
                                <a href="#" class="btn">Learn More <span class="arrow"></span></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="event_wrap d-flex align-items-center">
                            <div class="event_image">
                                <img src="images/event_4.jpg" alt="" />
                            </div>
                            <div class="event_info">
                                <span class="date">September 12, 2018</span>
                                <a href="#" class="name">Indiana IT Symposium</a>
                                <span class="location"><i class="fas fa-map-marker-alt"></i> Indianapolis, IN</span>
                                <a href="#" class="btn">Learn More <span class="arrow"></span></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="event_wrap d-flex align-items-center">
                            <div class="event_image">
                                <img src="images/event_5.jpg" alt="" />
                            </div>
                            <div class="event_info">
                                <span class="date">September 12, 2018</span>
                                <a href="#" class="name">Indiana IT Symposium</a>
                                <span class="location"><i class="fas fa-map-marker-alt"></i> Indianapolis, IN</span>
                                <a href="#" class="btn">Learn More <span class="arrow"></span></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="event_wrap d-flex align-items-center">
                            <div class="event_image">
                                <img src="images/event_6.jpg" alt="" />
                            </div>
                            <div class="event_info">
                                <span class="date">September 12, 2018</span>
                                <a href="#" class="name">Indiana IT Symposium</a>
                                <span class="location"><i class="fas fa-map-marker-alt"></i> Indianapolis, IN</span>
                                <a href="#" class="btn">Learn More <span class="arrow"></span></a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div> <!-- Events Listing -->
        </div> <!-- container -->

        <?php include("footer.php"); ?>

    </body>
</html>
