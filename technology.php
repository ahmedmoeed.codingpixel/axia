<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Technology </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/homebanner2.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner10.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <span class="cus_animate fromRight">AXIA IS</span>
                                    <h2 class="cus_animate fromRight">Technology services that <br/>help you maximize your <br/>enterprise investments <br/>for real results</h2>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->


        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">What we Do<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Thought Leadership<span class="arrow"></span></a>
                </div>
            </div>
        </div> <!-- container-->

        <div class="categories_drop_down">
            <div class="contentContainer">
                <div class="row">
                    <div class="col">
                        <div class="parent_menu_item">
                            <span class="open_cat">Industries Served <i class="fas fa-angle-down"></i></span>
                        </div>
                        <div class="categories_list col-10 offset-md-1">
                            <ul class="d-flex flex-wrap">
                                <li><a href="#">Technology</a></li>
                                <li><a href="#">Manufacturing & Supply Chain</a></li>
                                <li><a href="#">Utilities & Energy</a></li>
                                <li><a href="#">Government & Higher Education</a></li>
                                <li><a href="#">Retail & eCommerce</a></li>
                                <li><a href="#">Financial Services</a></li>
                                <li><a href="#">Healthcare</a></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- categories_drop_down -->

        <div class="industry_intro">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-7">
                        <div class="left_side">
                            <div class="txt_content">
                                <div class="section_title">
                                    <h2>Technology</h2>
                                </div>
                                <p>The technology industry has to manage the ever-increasing demands of customers, global supply chains, outside vendors, revenue recognition, mergers, and divestitures.</p>
                                <p>AXIA Consulting’s Technology Industry Practice has extensive experience in the technology industry. AXIA’s consultants have an average of 20 years of hands on experience helping clients quickly work through the challenges that face the technology providers.</p>
                                <p>Whether your project is in the concept, in-flight, or support phase, our consultants can partner with your business and IT staff to deliver the best business value.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="tech_image_overlapping">
                            <div class="image1">
                                <img src="images/image8.jpg" alt="" class="img-fluid" />
                                <div class="top_img">
                                    <img src="images/image9.jpg" alt="" class="img-fluid" />    
                                </div>
                                <div class="left_img">
                                    <img src="images/image7.jpg" alt="" class="img-fluid" />    
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- industry_intro -->

        <!-- Brain Portion -->
        <div class="thought_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Thought Leadership</h2>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="text_content">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <br/><br/>
                            <a href="#" class="btn">READ FULL ARTICLE <span class="arrow"></span></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 d-md-block d-lg-block d-sm-none d-none">
                        <div class="thought_img">
                            <img src="images/image10.jpg" alt="" class="img-right" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brain Portion ends -->

        <div class="tech_services bg_grey">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Our services include:</h2>
                        </div>
                        <ul class="list col2 list_dark list-unstyled">
                            <li>ERP Implementations & Upgrades</li>
                            <li>Business Intelligence Solutions</li>
                            <li>Enterprise Content Management</li>
                            <li>Program Management</li>
                            <li>IT Strategy</li>
                            <li>Post-Merger Integration</li>
                            <li>Business Transformation & Process Improvement</li>
                            <li>Software Selection & Application Portfolio Rationalization</li>
                        </ul>
                    </div>
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- industry_intro -->

        <div class="full_img_container">
            <div class="w_bg">
                <div class="contentContainer">
                    <img src="images/image24.jpg" alt="" class="img-fluid" />
                </div>
            </div>
        </div>

        <div class="industry_specialisation_sec">
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col-12">
                            <div class="heading_style">
                                <h2>AXIA’s consultants have extensive experience handling <br/>
                                    the problems unique to the technology industry, including:</h2>
                            </div>
                            <ul class="list col2 list-unstyled">
                                <li>Revenue Recognition / VSOE</li>
                                <li>Advanced Pricing & Product Configuration</li>
                                <li>Recurring Revenue & Service Contract</li>
                                <li>Lot Tracking & Genealogy</li>
                                <li>Software License Key Tracking</li>
                                <li>Contract Manufacturing</li>
                                <li>Contract Management</li>
                                <li>Global Client Management</li>
                                <li>Customer Support Management</li>
                                <li>International Manufacturing Requirements</li>
                                <li>Global Supply Chain Planning & Execution</li>
                                <li>Program Management</li>
                                <li>Cloud Computing</li>
                            </ul>
                        </div> <!-- col -->
                    </div> <!-- row -->
                </div> <!-- container-->
            </div> <!-- overlay-->
        </div> <!-- industry_specialisation_sec -->

        <div class="newsEvents_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Other Services</h2>
                        </div>
                    </div> <!-- col -->
                    <div id="newsEvents_counter"></div> 
                    <div class="newsEvents owl-carousel owl-theme">
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/image1.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a></h6>
                                    <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global firm where he served as an Account Manager and worked with.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>                       
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-2.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA named Cincinnati Best Place to Work, Second Consecutive Year</a></h6>
                                    <p>Cincinnati, OH ��AXIA Consulting announces that for the second year in a row, the company has been recognized as one of the Best Places to Work by the Cincinnati Business Courier. To be nominated,.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-3.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Releases Book on Improving Communication Between Project Managers and Developers</a></h6>
                                    <p>COLUMBUS, OH (November 30, 2016) � AXIA Consulting, a global provider of business and technology solutions, announced the release of its book, Perfect Strangers: How Project Managers and Developers Relate and Succeed. Written by AXIA</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                    </div> <!-- carousel END -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- newsEvents_section -->

        <div class="call_action mb-0">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>To learn more about our industry-specific expertise, <br/>visit our seven industry pages or contact us at <a href="#">877-292-5503.</a></p>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- call_action -->

        <?php include("footer.php"); ?>

    </body>
</html>
