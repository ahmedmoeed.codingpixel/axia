<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Culture </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner7.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner9.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <h2 class="cus_animate fromRight">Culture</h2>
                                    <p class="cus_animate fromRight">Down-to-earth culture rooted in core values</p>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->
        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">Careers<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Community Involvement<span class="arrow"></span></a>
                </div>
            </div>
        </div>

        <div class="content_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>At AXIA, our culture is central to who we are and how we do business. We pride ourselves on providing the expertise of a global consulting firm with accessible, down-to-earth service. At the heart of our culture is a commitment to our core values:</p>
                        <ul class="time_line">
                            <li>Be Vested</li>
                            <li>Be Authentic</li>
                            <li>Be There</li>
                            <li>Be Approachable</li>
                            <li>Be Honest</li>
                        </ul>
                        <br/><br/>
                        <p>We make decisions based on our values and their impact on people, placing high importance on our relationships with colleagues and clients. We strive to be accessible to everyone we work with and serve as problem solvers who lead with a collaborative approach. Our environment is flexible, spontaneous and ever-changing. We put high emphasis on creativity, yet we are very practical in our recommendations, founding them on information that is factual, real and current.</p>                        
                        <p>By attracting, developing and retaining top talent, our team is able to create impactful business and technology solutions for our clients. We empower our consultants to independently manage their own work and encourage collaboration among members of our team in order to efficiently deliver solutions.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="call_action_careers">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-lg-8 col-12">
                        <h2>Career Opportunities</h2>
                        <p>For more information about employment opportunities.</p>
                    </div>
                    <div class="col-lg-4">
                        <a href="#" class="btn btn-white">GO TO CAREERS<span class="arrow"></span></a>
                    </div>
                </div>
            </div>
        </div>

        <?php include("footer.php"); ?>

    </body>
</html>
