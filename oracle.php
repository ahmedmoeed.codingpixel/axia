<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Oracle </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner9.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner10.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <h2 class="cus_animate fromRight">Oracle Services</h2>
                                    <p class="cus_animate fromRight">Our Oracle leaders deliver proven results for your toughest business challenges.</p>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->

            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="banner_links">
            <div class="contentContainer">
                <div class="left">
                    <a href="#" class="link">What we Do<span class="arrow"></span></a>
                </div>
                <div class="right">
                    <a href="#" class="link">Thought Leadership<span class="arrow"></span></a>
                </div>
            </div>
        </div>

        <div class="content_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="oracle_logo_wrap float-md-left float-none">
                            <img src="images/oracle-gold-partner.png" alt="iamge" />
                        </div>                        
                        <p>Your enterprise applications are essential for operating a successful business. When it comes to selecting, implementing, integrating or maintaining an Oracle solution, you want to make sure it’s done right, with as little interruption to your business as possible.</p>
                        <p>That’s where we come in. With more than two decades of hands-on Oracle experience, AXIA consultants have the knowledge, resources and proven methodologies needed to ensure your Oracle project delivers bottom-line results. We’ve successfully developed, implemented and maintained Oracle solutions in numerous environments and industries. In fact, we often take on the most challenging projects – from post-merger system integrations, to multinational consolidation or rollouts.</p>

                        <div class="clearfix"></div>
                        <h2>Our Approach</h2><br/>
                        <p>Today’s enterprise applications are essential for operating a successful business. The journey from program kickoff to realizing bottom line results is a transformation that includes new technologies, new processes and revised organizational capabilities.</p>
                        <p>AXIA Consulting’s Oracle Application and Development Practice has the highly-skilled resources, proven methodologies and deep experience to guide your Oracle Implementation Program to success. AXIA’s Oracle consultants average over 20 years of hands on experience and are ready to help your program deliver bottom line results.</p>
                        <p>We take the time to understand your organization, delivering solutions that effectively leverage Oracle to make the most impact. The AXIA Oracle Value Delivery Methodology then ensures solutions are implemented quickly and efficiently. This advanced methodology, aligned with the Oracle Application Implementation Methodology (AIM), allows our team to anticipate needs, identify gaps and quickly overcome unforeseen challenges. This approach is what sets AXIA apart, as it requires the leadership and experience that only a seasoned business consultant can provide.</p>
                        <p>Whether you need counsel on selecting the right Oracle application, help with system implementation, or want to upgrade or extend your current Oracle investment, we’re here to serve as an extension of your team and guide your project to success.</p>
                        <br/>

                    </div>
                </div>
            </div>
        </div>

        <!-- Brain Portion -->
        <div class="thought_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Thought Leadership</h2>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="text_content">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <br/><br/>
                            <a href="#" class="btn">READ FULL ARTICLE <span class="arrow"></span></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 d-md-block d-lg-block d-sm-none d-none">
                        <div class="thought_img">
                            <img src="images/image10.jpg" alt="" class="img-right" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brain Portion ends -->

        <div class="content_section bg_grey">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Oracle Solutions Delivered by AXIA Consulting:</h2>
                        </div>
                        <ul class="list col3 list_dark list-unstyled">
                            <li>Consulting</li>
                            <li>Implementations</li>
                            <li>Program Management</li>
                            <li>Upgrades & Functional Extensions</li>
                            <li>Technical Services</li>
                            <li>EBS Assessments & Roadmaps</li>
                            <li>Mergers & Acquisitions</li>
                            <li>Multinational Rollouts</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="newsEvents_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Other Services:</h2>
                        </div>
                    </div> <!-- col -->
                    <div id="newsEvents_counter"></div> 
                    <div class="newsEvents owl-carousel owl-theme">
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/image1.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a></h6>
                                    <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global firm where he served as an Account Manager and worked with.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>                       
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-2.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA named Cincinnati Best Place to Work, Second Consecutive Year</a></h6>
                                    <p>Cincinnati, OH ��AXIA Consulting announces that for the second year in a row, the company has been recognized as one of the Best Places to Work by the Cincinnati Business Courier. To be nominated,.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-3.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Releases Book on Improving Communication Between Project Managers and Developers</a></h6>
                                    <p>COLUMBUS, OH (November 30, 2016) � AXIA Consulting, a global provider of business and technology solutions, announced the release of its book, Perfect Strangers: How Project Managers and Developers Relate and Succeed. Written by AXIA</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                    </div> <!-- carousel END -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- newsEvents_section -->

        <div class="call_action mb-0">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>To learn more about our industry-specific expertise, <br>visit our seven industry pages or contact us at <a href="#">877-292-5503.</a></p>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container-->
        </div>

        <?php include("footer.php"); ?>

    </body>
</html>
