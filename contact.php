<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Contact </title>
        <?php include("assets.php"); ?>
    </head>

    <body>
        <?php include("header-contact.php"); ?>

        <div class="borderRight border_style"></div>

        <div class="contact_banner d-flex" style="background-image: url('images/image25.jpg');">
            <div class="contentContainer">
                <div class="banner_content">
                    <span class="jump_arrow"></span>
                </div>
            </div> <!-- container-->
        </div> <!-- banner -->

        <div class="contact_section">
            <div class="contentContainer">
                <div class="row flex-row-reverse">
                    <div class="col-md-6">
                        <div class="contactform">
                            <form>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your email">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Message"></textarea>
                                </div>
                                <div class="form-group mt-4">
                                    <input type="submit" class="btn" value="Send">
                                </div>
                            </form>
                        </div>
                    </div> <!-- col -->
                    <div class="col-md-6">
                        <div class="contact_short_info">
                            <p><strong>Is your organization facing a complex business <br/> or technology challenge? Connect with our <br/> senior consultants today!</strong></p>
                            <p>To learn more about how AXIA can help your company overcome its business and technology challenges, please contact our Columbus headquarters or your local office.</p>
                            <ul class="social_icon bordered list-unstyled mt-4">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->

                <div class="branches">

                    <ul class="row list-unstyled">
                        <li class="">
                            <div class="branch_info">
                                <div class="image">
                                    <img src="images/branch_1.jpg" alt="" />
                                </div>
                                <div class="branch_detail">
                                    <h5 class="name">Headquarters – Columbus</h5>
                                    <div class="address">1391 West Fifth Avenue Suite 320 Columbus, OH 43212</div>
                                    <div class="contact">
                                        <strong>Toll Free:</strong> 866-937-5550 <br/>
                                        <strong>Local:</strong> 614-675-4050 <br/>
                                        <strong>Email:</strong> info@axiaconsulting.net
                                    </div>
                                </div>
                            </div> <!-- branch_info -->
                        </li> <!-- col -->
                        <li class="">
                            <div class="branch_info">
                                <div class="image">
                                    <img src="images/branch_2.jpg" alt="" />
                                </div>
                                <div class="branch_detail">
                                    <h5 class="name">Cincinnati</h5>
                                    <div class="address">2692 Madison Road Suite N1-387 Cincinnati, OH 45208</div>
                                    <div class="contact">
                                        <strong>Local:</strong> 513-823-2127 <br/>
                                    </div>
                                </div>
                            </div> <!-- branch_info -->
                        </li> <!-- col -->
                        <li class="">
                            <div class="branch_info">
                                <div class="image">
                                    <img src="images/branch_3.jpg" alt="" />
                                </div>
                                <div class="branch_detail">
                                    <h5 class="name">Cleveland</h5>
                                    <div class="address">13940 Cedar Road Suite 389 Cleveland OH 44118</div>
                                    <div class="contact">
                                        <strong>Local:</strong> 440-792-6400 <br/>
                                    </div>
                                </div>
                            </div> <!-- branch_info -->
                        </li> <!-- col -->
                        <li class="">
                            <div class="branch_info">
                                <div class="image">
                                    <img src="images/branch_4.jpg" alt="" />
                                </div>
                                <div class="branch_detail">
                                    <h5 class="name">Atlanta</h5>
                                    <div class="address">12460 Crabapple Road Suite 202-340 Alpharetta, GA 30004</div>
                                    <div class="contact">
                                        <strong>Local:</strong> 678-444-4550 <br/>
                                    </div>
                                </div>
                            </div> <!-- branch_info -->
                        </li> <!-- col -->
                        <li class="">
                            <div class="branch_info">
                                <div class="image">
                                    <img src="images/branch_5.jpg" alt="" />
                                </div>
                                <div class="branch_detail">
                                    <h5 class="name">Indianapolis</h5>
                                    <div class="address">9801 Fall Creek Road #240 Indianapolis, IN 46256</div>
                                    <div class="contact">
                                        <strong>Local:</strong> 317-451-4185 <br/>
                                    </div>
                                </div>
                            </div> <!-- branch_info -->
                        </li> <!-- col -->
                        <li class="">
                            <div class="branch_info">
                                <div class="employment_opportunities">
                                    <p>To learn more about employment pportunities, please visit our <a href="#"> Careers </a>page.</p>
                                    <img src="images/image26.jpg" alt="" />
                                </div>
                            </div> <!-- branch_info -->
                        </li> <!-- col -->
                        <li class="">
                            <div class="branch_info">
                                <div class="image">
                                    <img src="images/branch_6.jpg" alt="" />
                                </div>
                                <div class="branch_detail">
                                    <h5 class="name">Pittsburgh</h5>
                                    <div class="address">The address of the location will come here</div>
                                    <div class="contact">
                                        <strong>Local:</strong> xxx-xxx-xxxx <br/>
                                    </div>
                                </div>
                            </div> <!-- branch_info -->
                        </li> <!-- col -->
                    </ul>
                </div> <!-- row -->
            </div> <!-- branches -->
        </div> <!-- container-->

        <div class="call_action mb-0">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <p>To learn more about our industry-specific expertise, <br>visit our seven industry pages or contact us at <a href="#">877-292-5503.</a></p>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container-->
        </div>        

        <?php include("footer.php"); ?>

    </body>
</html>
