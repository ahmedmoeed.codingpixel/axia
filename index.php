<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | Home Page </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>

        <div class="home_banner">
            <div class="owl-carousel bannerCarousel owl-theme">
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/homebanner.jpg')">
                    </div>
                </div>
                <div class="item">
                    <div class="slide_img" style="background-image: url('images/banner8.jpg')">
                    </div>
                </div>
            </div>
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col">
                            <div class="banner_content">
                                <div class="content">
                                    <span class="cus_animate fromRight">AXIA IS</span>
                                    <h2 class="cus_animate fromRight">Business & <br/> Technology <br/>Simplified.</h2>
                                </div>
                                <span class="jump_arrow"></span>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->
            </div> <!-- overlay-->
        </div> <!-- banner -->

        <div class="contentContainer">
            <div class="banner_overlap_links">
                <div class="row">
                    <div class="col-lg-5 col-sm-6">
                        <div class="lnk_about cus_animate fromBottom">
                            <div class="overlay">
                                <a href="#" class="link">What we do<span class="arrow"></span></a>
                                <div class="text_content">
                                    <img src="images/x.png" alt="image" />
                                    <p>At AXIA Consulting, we understand the importance of choosing a trustworthy partner who takes the time to understand your business and has the proven ability to deliver meaningful results.</p>
                                </div>
                            </div>
                        </div>                            
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="lnk_service cus_animate fromBottom">
                            <a href="#" class="link">Why AXIA?<span class="arrow"></span></a>
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- banner_overlap_links -->
        </div>

        <div class="ourservices_section jump_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>What We Do</h2>
                        </div>
                        <ul class="services_tab_list list-unstyled clearfix">
                            <li data-num="1" class="active"><a href="#">Cloud</a></li>
                            <li data-num="2"><a href="#">Technology</a></li>
                            <li data-num="3"><a href="#">Data</a></li>
                            <li data-num="4"><a href="#">Business Consulting</a></li>
                            <li data-num="5"><a href="#">Organizational Change Management</a></li>
                            <li data-num="6"><a href="#">M & A</a></li>
                            <li data-num="7"><a href="#">Program Management </a></li>
                            <li data-num="8"><a href="#">Innovation</a></li>
                        </ul>
                        <div class="services_tab_slider">
                            <div class="service_images">
                                <img src="images/services-spacer.png" class="spacer">
                                <div class="service_image show" data-num="1">
                                    <img src="images/banner4.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="2">
                                    <img src="images/banner5.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="3">
                                    <img src="images/banner6.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="4">
                                    <img src="images/banner7.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="5">
                                    <img src="images/banner8.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="6">
                                    <img src="images/banner9.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="7">
                                    <img src="images/banner10.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="service_image" data-num="8">
                                    <img src="images/banner11.jpg" alt="image" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="service_small_images">
                                <div class="image show" data-num="1">
                                    <img src="images/banner5.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="2">
                                    <img src="images/banner6.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="3">
                                    <img src="images/banner7.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="4">
                                    <img src="images/banner8.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="5">
                                    <img src="images/banner9.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="6">
                                    <img src="images/banner10.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="7">
                                    <img src="images/banner11.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="image" data-num="8">
                                    <img src="images/banner12.jpg" alt="image" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="services_text">
                                <div class="text show" data-num="1">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="2">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="3">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="4">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="5">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="6">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="7">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="text" data-num="8">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                        </div>
                                        <div class="col-4">
                                            <a href="#" class="btn float-right">LEARN MORE<span class="arrow"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sliding-cover"></div>
                        </div>

                        <div class="servicesMobile">
                            <div class="service">
                                <div class="image">
                                    <img src="images/banner4.jpg" alt="image" class="img-fluid"/>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <p>AXIA Client Advisory services equip your organization with the tools needed to navigate your critical projects, IT challenges and organizational changes. Whether your needs are local or global, we take the time to understand the intricacies of your organization and apply our industry-focused approach to your specific needs.</p>
                                    </div>
                                    <div class="col-4">
                                        <a href="#" class="btn">LEARN MORE<span class="arrow"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- Our Services section -->

        <div class="thought_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Thought Leadership</h2>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="text_content cus_animate fromLeft">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis </p>
                            <br/>
                            <a href="#" class="btn">READ FULL ARTICLE <span class="arrow"></span></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5 d-md-block d-lg-block d-sm-none d-none">
                        <div class="thought_img cus_animate fromRight">
                            <img src="images/image10.jpg" alt="" class="img-right" />
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="industries_served_section border_style">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>Industries Served</h2>
                            <ul class="industries_categories d-flex flex-wrap list-unstyled clearfix">
                                <li class="active"><a href="#">All</a></li>
                                <li><a href="#">Manufacturing & Supply Chain</a></li>
                                <li><a href="#">Government and Higher Education</a></li>
                                <li><a href="#">High Tech</a></li>
                                <li><a href="#">Healthcare</a></li>
                                <li><a href="#">Utilities & Energy</a></li>
                                <li><a href="#">Financial Services</a></li>
                                <li><a href="#">Agriculture</a></li>
                                <li><a href="#">Retail</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> <!-- contentContainer -->            
            <div class="container">
                <div class="row">
                    <div class="industry_service_row first flex-wrap">
                        <div class="col-sm-7 col-xs-12 d-flex align-items-end">
                            <div class="industry_service reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect">
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/industry-1.jpg" alt="image" class="img-fluid"/>
                                    </div>
                                </a>
                                <div class="short_info">
                                    <a href="#"><h3>Technology</h3></a>
                                    <p>Technology services that help you maximize your enterprise investments for real results</p>
                                </div>
                                <div class="colorLayerTop"></div>
                            </div>
                        </div> <!-- col -->
                        <div class="col-sm-5 col-xs-12 d-flex align-items-end">
                            <div class="industry_service reveal_wrap cus_animate"> 
                                <a href="#">
                                    <div class="image img_hover_effect">
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/industry-2.jpg" alt="image" class="img-fluid"/>
                                    </div>
                                </a>
                                <div class="short_info">
                                    <a href="#"><h3>Manufacturing & Supply Chain</h3></a>
                                    <p>Global solutions to optimize operations and keep you running efficiently</p>
                                </div>
                                <div class="colorLayerTop"></div>
                            </div>
                        </div> <!-- col -->                        
                    </div> <!-- row -->
                    <div class="industry_service_row second flex-wrap">
                        <div class="col-md-4 col-sm-5 col-12">
                            <div class="industry_service reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect">
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/industry-3.jpg" alt="image" class="img-fluid"/>
                                    </div>
                                </a>
                                <div class="short_info">
                                    <a href="#"><h3>Utilities & Energy</h3></a>
                                    <p>Technology solutions to power your business</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- col -->
                        <div class="col-md-4 col-sm-5 col-12">
                            <div class="industry_service reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect">
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/industry-4.jpg" alt="image" class="img-fluid"/>
                                    </div>
                                </a>
                                <div class="short_info">
                                    <a href="#"><h3>Government & Higher Education</h3></a>
                                    <p>Leveraging years of public sector experience to solve your toughest business and technology challenges</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- col -->
                    </div> <!-- row -->
                    <div class="industry_service_row third flex-wrap">
                        <div class="col-sm-8 col-12">
                            <div class="industry_service reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect">
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/industry-5.jpg" alt="image" class="img-fluid"/>
                                    </div>
                                </a>
                                <div class="short_info">
                                    <a href="#"><h3>Retail & E-Commerce</h3></a>
                                    <p>Global retail and e-commerce solutions to meet consumer demand and increase speed-to-market</p>
                                </div>
                                <div class="colorLayerTop"></div>
                            </div>
                        </div> <!-- col -->
                    </div>  <!-- row -->
                    <div class="contentContainer">
                        <a href="#" class="btn btn-white btn_projets">thought leadership <span class="arrow"></span></a>
                    </div>
                </div> <!-- row -->
            </div> <!-- container-->
        </div> <!-- about us section -->

        <div class="aboutus_section border_style">
            <div class="overlay">
                <div class="contentContainer">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="image cus_animate fromBottom">
                                <img src="images/image1.jpg" alt="image" class="img-fluid"/>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 d-flex align-items-center">
                            <div class="about-content cus_animate fromTop">
                                <div class="heading_style">
                                    <h2>Grow with <br/> AXIA Consulting</h2>
                                </div>
                                <div class="txt_content">
                                    <p>AXIA Consulting brings powerful technology and business expertise to its clients to solve their most complex challenges in industries ranging from government and higher education to healthcare and manufacturing.</p>
                                </div>
                                <a href="#" class="btn btn-white">Explore Careers <span class="arrow"></span></a>
                            </div>
                        </div>
                    </div> <!-- row -->
                </div> <!-- container-->
            </div> <!-- overlay-->
        </div> <!-- about us section -->

        <div class="newsEvents_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title">
                            <h2>AXIA on the Move</h2>
                        </div>
                    </div> <!-- col -->
                    <div id="newsEvents_counter"></div> 
                    <div class="newsEvents owl-carousel owl-theme">
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/image1.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Consulting Introduces Nick Lemoine as the Newest Member of its Business Development Team</a></h6>
                                    <p>AXIA Consulting would like to welcome Nick Lemoine to the AXIA Consulting business development team. Nick is joining AXIA from a Global firm where he served as an Account Manager and worked with.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>                       
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-2.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA named Cincinnati Best Place to Work, Second Consecutive Year</a></h6>
                                    <p>Cincinnati, OH ��AXIA Consulting announces that for the second year in a row, the company has been recognized as one of the Best Places to Work by the Cincinnati Business Courier. To be nominated,.....</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                        <div class="item">
                            <div class="c_news reveal_wrap cus_animate">
                                <a href="#">
                                    <div class="image img_hover_effect" style=" background-image: url('images/new-3.jpg')" >
                                        <div class="over_effect">
                                            <span>View More</span>
                                        </div>
                                        <img src="images/spacer-new-slider.png" alt="spacer" />
                                    </div>
                                </a>
                                <div class="short-description">
                                    <h6><a href="#">AXIA Releases Book on Improving Communication Between Project Managers and Developers</a></h6>
                                    <p>COLUMBUS, OH (November 30, 2016) � AXIA Consulting, a global provider of business and technology solutions, announced the release of its book, Perfect Strangers: How Project Managers and Developers Relate and Succeed. Written by AXIA</p>
                                </div>
                                <div class="colorLayerLeft"></div>
                            </div>
                        </div> <!-- item -->
                    </div> <!-- carousel END -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- newsEvents_section -->

        <div class="testimonial_section">
            <div class="contentContainer">
                <div class="row">
                    <div class="col">
                        <div class="inner_wrap cus_animate fromTop">
                            <h3>Offerings </h3>
                            <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velitNeque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velitNeque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                            <a href="#" class="btn btn-white">LEARN MORE <span class="arrow"></span></a>
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- subscribe_section -->

        <?php include("footer.php") ?>

    </body>
</html>