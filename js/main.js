$(document).ready(function () {
    
    setTimeout(function () {
        $('.loadingOpen').addClass('start');
    }, 1500);
    
    $('.services_tab_list li a').click(function (e) {
        e.preventDefault();
        $('.sliding-cover').addClass('show');
        var parentLi = $(this).parent('li');
        var number = parentLi.attr('data-num');
        parentLi.siblings('li').removeClass('active');
        parentLi.addClass('active');
        setTimeout(function () {
            $('.service_images .service_image').removeClass('show');
            $('.service_images .service_image[data-num="' + number + '"]').addClass('show');
            $('.service_small_images .image').removeClass('show');
            $('.service_small_images .image[data-num="' + number + '"]').addClass('show');
            $('.services_text .text').removeClass('show');
            $('.services_text .text[data-num="' + number + '"]').addClass('show');
        }, 700);
        setTimeout(function () {
            $(".sliding-cover").removeClass('show');
        }, 1700);
    });

    if ($('.newsEvents').length > 0) {
        $('.newsEvents').owlCarousel({
            nav: true,
            loop: true,
            margin: 10,
            items: 3,
            navText: ['<span class="arrow black left"></span>', '<span class="arrow black"></span>'],
            onInitialized: counter,
            onTranslated: counter,
            responsive: {
                0: {
                    items: 1
                },
                576: {
                    items: 2
                },
                850: {
                    items: 3
                }
            }
        });
    }

    function counter(event) {
        var element = event.target;         // DOM element, in this example .owl-carousel
        var items = event.item.count;     // Number of items
        var item = event.item.index + 1;     // Position of the current item
        // it loop is true then reset counter from 1
        if (item > items) {
            item = item - items;
        }
        $('#newsEvents_counter').html("0" + item + "<span>/</span>" + "0" + items);
    }

    if ($('.bannerCarousel').length > 0) {
        $('.bannerCarousel').owlCarousel({
            items: 1,
            animateOut: 'fadeOut',
            loop: true,
            margin: 10,
            nav: false,
            pagination: false,
            dots: false,
            autoplay: true,
            mouseDrag: false
        });
    }


    function animScroll(sec, speed, offset) {
        activeOffset = $(sec).offset().top;
        TweenMax.to('html,body', speed, {scrollTop: activeOffset - offset, ease: Expo.easeInOut});
    }

    $(window).scroll(function () {
        if ($(window).scrollTop() > 5) {
            if (!($('.header').hasClass('header_sticky'))) {
                $('.main_menu, .header').addClass('hasSticky');
                $('.header img').attr('src', 'images/logo_hover.png');
            }
        } else {
            if (!($('.header').hasClass('header_sticky'))) {
                $('.main_menu, .header').removeClass('hasSticky');
                $('.header img').attr('src', 'images/logo.png');
            }
        }
    });

    $('.open_cat').click(function () {
        $('.categories_list').slideToggle();
    });


    $('.banner_content').find('.jump_arrow').click(function () {
        animScroll($('.jump_section'), .75, 250);
    });

    $('.menu_btn').click(function () {
        $(this).toggleClass('open');
        $('.main_menu').toggleClass('open');
    });
    jQuery('.cus_animate').each(function () {
        var windowHeight = $(window).height();
        if (jQuery(this).offset().top < windowHeight) {
            jQuery(this).addClass('animateStart');
        }
    });

    jQuery(window).scroll(function () {
        jQuery('.cus_animate').each(function () {
            var windowScroll = jQuery(window).scrollTop() + (jQuery(window).innerHeight() * 0.5);
            if (jQuery(this).offset().top < windowScroll) {
                jQuery(this).addClass('animateStart');
            }
        });
    });

    $(document).click(function (e) {
        var left = e.pageX;
        var top = e.pageY;
        var mouseCursor = '<div class="cursor" style="left:' + left + 'px; top:' + top + 'px;"></div>';
        $('body').append(mouseCursor);
        setTimeout(function () {
            $('.cursor').addClass('zoom');
        }, 10);
        setTimeout(function () {
            $(".cursor").remove();
        }, 300);
    });

    $('a').click(function (e) {
        var href = $(this).attr('href');
        if (href !== '#') {
            e.preventDefault();
            $('.loadingClose').addClass('start');
            setTimeout(function () {
                window.location.href = href;
            }, 2500);
        }
    });

});

window.onload = function () {
    setTimeout(function () {
        $('.page_loader').hide();
    }, 2000);
};