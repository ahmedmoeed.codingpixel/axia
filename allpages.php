<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> AXIA | LeaderShip </title>
        <?php include("assets.php"); ?>
    </head>

    <body>

        <?php include("header.php"); ?>
        <br/><br/><br/><br/>
        <section>
            <ul>
                <li><a href="index.php">Home Page</a></li>
                <li><a href="industries.php">Industries</a></li>
                <li><a href="technology.php">Technology</a></li>
                <li><a href="manufacturing.php">Manufacturing & Supply Chain</a></li>
                <li><a href="clientAdvisory.php">Client Advisory</a></li>
                <li><a href="optimizing.php">Optimizing Business Performance with a Program Management Offic</a></li>
                <li><a href="cloud.php">Cloud</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="oracle.php">Oracle</a></li>
                <li><a href="careers.php">Careers</a></li>
                <li><a href="culture.php">Culture</a></li>
                <li><a href="leadership.php">Leadership</a></li>
                <li><a href="events.php">Events</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="newsSingle.php">News Inner</a></li>
            </ul>
        </section><br/><br/><br/>
        <?php include("footer.php") ?>